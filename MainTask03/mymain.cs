using System;
using System.IO;
using SimpleScanner;
using ScannerHelper;
using System.Linq;


namespace Main
{
    class mymain
    {
        //private int idCount = 0;

        static void Main(string[] args)
        {
            // ����� ������������ ����� �������������� � ������������ � ������� 3.14 (� �� 3,14 ��� � ������� Culture)
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            var fname = @"..\..\a.txt";
            Console.WriteLine(File.ReadAllText(fname));
            Console.WriteLine("-------------------------");

            Scanner scanner = new Scanner(new FileStream(fname, FileMode.Open));

            int tok = 0;
            do {
                tok = scanner.yylex();
                if (tok == (int)Tok.EOF)
                    break;
                Console.WriteLine(scanner.TokToString((Tok)tok));
            } while (true);
            

            var sb = new System.Text.StringBuilder();

            sb.AppendFormat("Integers sum: {0}", scanner.iSum)
              .AppendLine()
              .AppendFormat("Reals sum: {0}", scanner.rSum);

            Console.WriteLine(sb.ToString());

            

            Console.ReadKey();
        }
    }
}
