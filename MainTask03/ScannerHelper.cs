﻿namespace ScannerHelper
{
    public enum Tok { EOF = 0, ID, INUM, RNUM, COLON, SEMICOLON, ASSIGN, BEGIN, END, CYCLE, ONE_LINE_COMMENT, SINGLE_QOUTED_STRING, MULTILINE_COMMENT };
}