%using ScannerHelper;
%using System.Linq;
%namespace SimpleScanner

Alpha 	[a-zA-Z_]
Digit   [0-9] 
AlphaDigit {Alpha}|{Digit}
INTNUM  {Digit}+
REALNUM {INTNUM}\.{INTNUM}
ID {Alpha}{AlphaDigit}* 

DotChr [^\r\n]
OneLineCmnt  \/\/{DotChr}*

SingleQuotedString \'[^']*\'

// ����� ����� ������ �������� �����, ���������� � ������� - ��� �������� � ����� Scanner
%{
  public int LexValueInt;
  public double LexValueDouble;

  public ICollection<int> idLengths = new List<int>();
  public ICollection<string> commentIds = new List<string>();
  public int iSum = 0;
  public double rSum = 0;
%}

%x COMMENT

%%
{OneLineCmnt} {
  return (int)Tok.ONE_LINE_COMMENT;
}

{SingleQuotedString} {
  return (int)Tok.SINGLE_QOUTED_STRING;
}

{INTNUM} { 
  LexValueInt = int.Parse(yytext);
  iSum += LexValueInt;
  return (int)Tok.INUM;
}

{REALNUM} { 
  LexValueDouble = double.Parse(yytext);
  rSum += LexValueDouble;
  return (int)Tok.RNUM;
}

begin { 
  return (int)Tok.BEGIN;
}

end { 
  return (int)Tok.END;
}

cycle { 
  return (int)Tok.CYCLE;
}

{ID}  { 
  idLengths.Add(yytext.Length);
  return (int)Tok.ID;
}

":" { 
  return (int)Tok.COLON;
}

":=" { 
  return (int)Tok.ASSIGN;
}

";" { 
  return (int)Tok.SEMICOLON;
}

"{" { 
  commentIds.Clear();
  // ������� � ��������� COMMENT
  BEGIN(COMMENT);
}

<COMMENT> "}" { 
  // ������� � ��������� INITIAL
  BEGIN(INITIAL);
  return (int)Tok.MULTILINE_COMMENT;
}

<COMMENT>{ID} {
  // �������������� ID ������ �����������
  commentIds.Add(yytext);
  //return (int)Tok.ID;
}

[^ \r\n] {
	LexError();
	return 0; // ����� �������
}

%%

// ����� ����� ������ �������� ���������� � ������� - ��� ���� �������� � ����� Scanner

public void LexError()
{
	Console.WriteLine("({0},{1}): ����������� ������ {2}", yyline, yycol, yytext);
}

public string TokToString(Tok tok)
{
	switch (tok)
	{
		case Tok.ID:
			return tok + " " + yytext;
	    case Tok.SINGLE_QOUTED_STRING:
		    return tok + " " + yytext;
		case Tok.INUM:
			return tok + " " + LexValueInt;
		case Tok.RNUM:
			return tok + " " + LexValueDouble;
		case Tok.MULTILINE_COMMENT:
		{
		    return tok + " " + "IDs: " + string.Concat(commentIds.Select(x => x + " ")); 
		}
		default:
			return tok + "";
	}
}

